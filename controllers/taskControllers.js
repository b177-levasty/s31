const Task = require("../models/task");

// Controller function for getting all the tasks
module.exports.getAllTasks = () => {
	return Task.find({}).then(result => {
		return result;
	})
}

// Controller function for creating a task
module.exports.createTask = (requestBody) => {
	// Create a task object based on the mongoose model "Task"
	let newTask = new Task({
		// Stes the "name" property with the value received from the client
		name : requestBody.name
	})

	return newTask.save().then((task, error) => {
		if(error){
			console.log(error);
			return false;
		}
		// Save is successful
		else{
			return task;
		}
	})
}

// Create a controller function for deleting a task
module.exports.deleteTask = (taskId) => {
	return Task.findByIdAndRemove(taskId).then((removeTask, err) =>{
		if(err){
			console.log(err);
			return false;
		}
		else{
			return removeTask;
		}
	})
}

// Controller function for updating a task
module.exports.updateTask = (taskId, newContent) => {
	return Task.findById(taskId).then((result, error) => {
		if(error){
			console.log(err);
			return false
		}
		
		result.name = newContent.name;
		result.status = "complete";

		// Saves the updated result in the MongoDB database
		return result.save().then((updateTask, saveErr) => {
			if(saveErr){
				console.log(saveErr);
				return false;
			}
			else{
				return updateTask;
			}
		})
	})
}

// Activity

module.exports.updateStatus = (taskId, newContent) => {
	return Task.findById(taskId).then((resultStatus, error) => {
		if(error){
			console.log(err);
			return false
		}

		resultStatus.status = "completess";

		// Saves the updated result in the MongoDB database
		return resultStatus.save().then((updateStatus, saveErr) => {
			if(saveErr){
				console.log(saveErr);
				return false;
			}
			else{
				return updateStatus;
			}
		})
	})
}

/*module.exports.updateStatus = (taskId, newStatus) => {
	return Task.findById(taskIdStatus).then((result, error) => {
		if(error){
			console.log(err);
			return false
		}
		
		result.name = newContent.name;		
		result.status = "Pending"

		// Saves the updated result in the MongoDB database
		return result.save().then((updateStatus, saveErr) => {
			if(saveErr){
				console.log(saveErr);
				return false;
			}
			else{
				return updateStatus;
			}
		})
	})
}*/